const errorMessages = {
    'auth/user-not-found': 'Email não encontrado.',
    'auth/wrong-password': 'Senha inválida.',
    'auth/invalid-email': 'Email inválido.',
    'auth/too-many-requests':
        'Foram feitas muitas tentativas de login com esse email. Tente novamente mais tarde.',
    ' auth/invalid-password': 'Senha inválida.',
};

export default errorMessages;
