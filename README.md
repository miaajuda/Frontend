# Mia Ajuda 

É um projeto criado e desenvolvido por professores e estudantes da Faculdade do Gama (FGA), da Universidade de Brasília, com o intuito de contribuir com a sociedade em um momento de necessidade que estamos vivendo em relação à CoVid-19. O aplicativo tem o propósito de ser uma ferramenta de incentivo a ações sociais de ajuda e colaboração entre pessoas de comunidades e vizinhanças. O Mia Ajuda serve como um meio de ligação entre pessoas necessitadas e voluntários que possam ajudar, seja de forma imaterial (entretenimento, companhia, amparo psicológico), como de forma material (comida, objetos, itens de higiene pessoal).

# Ambiente 

Para desenvolver nesse projeto, é necessário a instalação do expo, que pode ser seguida por esse link: 
https://docs.expo.io/versions/latest/get-started/installation/

É necessário, também,  a instalação do expo em seu smartphone, ou um emulador configurado. 

# Executar o projeto no celular 

diriga-se para a pasta /app e execute o comando: 
### yarn start 

Após a inicialização, se bem sucedida, aparecerá o qrcode de conexão. Certifique-se de estar na mesma rede de seu computador, abra o app no seu smartphone e leia o qrcode;

# Executar o projeto no emulador 

diriga-se para a pasta /app e execute o comando: 
### yarn start 

Após a inicialização, se bem sucedida, aparecerá o qrcode de conexão. Abra o seu emulador e, no terminal onde está o qrcode, pressione o teclado 'a'. O app deve começar a carregar dentro do emulador a partir desse momento. 






